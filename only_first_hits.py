#!/usr/bin/env python
import sys

def only_first_hits(infile):
    first_hits=[]
    previous_queries=set()
    with open(infile,'r') as i:
        for line in i:
            query=line.split()[0].strip()
            subject=line.split()[1].strip()
            if query in previous_queries:
                continue
            else:
                previous_queries.add(query)
                first_hits.append(query+" "+subject) #strip used as a formating precaution
    return first_hits
    
def reciporical_hits(pairs1,pairs2):
    """Takes a list of pairs seprated by a space
        and returns those pairs from pairs1 if there is a reciporical
        pair in pairs2"""
    reciporical_hits=[]

    for pair in pairs1:
        #get the reciporical
        query=pair.split()[0]
        subject=pair.split()[1]
        reciporical=subject+" "+query
        if reciporical in pairs2:
            reciporical_hits.append(pair)
    return reciporical_hits

def write_to_file(results,outfile="best-reciporical-hits.txt"):
    with open(outfile,'w') as o:
        o.write("\n".join(results))
def main(blast_results1,blast_results2,outfile):
    pairs1=only_first_hits(blast_results1)
    pairs2=only_first_hits(blast_results2)
    results=reciporical_hits(pairs1,pairs2)
    write_to_file(results,outfile)

if __name__=="__main__":
    blast_results1=sys.argv[1]
    blast_results2=sys.argv[2]
    outfile=sys.argv[3]
    main(blast_results1,blast_results2,outfile)


