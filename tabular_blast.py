import shelve, pickle
class GiNotPresent(Exception):
    pass

class Tabular_Blast:
    def __init__(self, qseqid, sseqid, pident, length, mismatch, gapopen,
                 qstart,qend,sstart,send,evalue,bitscore):
        self.qseqid=qseqid #Query Seq-id
        self.sseqid=sseqid #Subject Seq-id
        self.pident=float(pident) #Percentage of identical matches
        self.length=int(length) #Alignment length 
        self.mismatch=int(mismatch) #Number of mismatches
        self.gapopen=int(gapopen)   #Number of gap openings
        self.qstart=int(qstart) #Start of alignment in query 
        self.qend=int(qend) #End of alignment in query
        self.sstart=int(sstart) #Start of alignment in subject
        self.send=int(send) #End of alignment in subject
        self.evalue=float(evalue) #evalue
        self.bitscore=float(bitscore) #bitscore

    def get_query_gi(self):
        try:
            return self.qseqid.split("|")[1]
        except IndexError:
            raise GiNotPresent

    def get_subject_gi(self):
        try:
            return self.sseqid.split("|")[1]
        except IndexError:
            raise GiNotPresent
    def tabular_tuple(self):
        return (self.qseqid,self.sseqid,self.pident,self.length,self.mismatch, \
                self.gapopen,self.qstart,self.qend,self.sstart,self.send, \
                self.evalue,self.bitscore)
    
    def __str__(self):
        variables=(self.qseqid,self.sseqid,str(self.pident),str(self.length),str(self.mismatch),
                   str(self.gapopen), str(self.qstart),str(self.qend),
                   str(self.sstart),str(self.send),str(self.evalue),
                   str(self.bitscore))
        return """ QueryID: %s+\n SubjectID: %s+\n PercentID: %s+\n Alignment Length: %s+\n Number of Mismatches: %s+\n Number of gap openings: %s+\n Start of alignment in query: %s+\n End of alignment in query: %s
 Start of alignment in subject: %s+\n End of alignment in subject %s+\n Evalue: %s+\n Bitscore %s+\n""" %variables

class Blast_Averages(Tabular_Blast):
    #Assumes have already ensured is a reciporical hit
    def __init__(self,record1,record2):
        self.hit_id=frozenset([record1.qseqid,record1.sseqid])
        self.avg_pident=(record1.pident*record2.pident)**0.5 #Geometric Mean
        self.avg_length=(record1.length+record2.length)/2
        self.avg_mismatch=(record1.mismatch+record2.mismatch)/2
        self.avg_gapopen=(record1.gapopen+record2.gapopen)/2
        self.avg_evalue=(record1.evalue+record2.evalue)/2
        self.avg_bitscore=(record1.bitscore+record2.bitscore)/2
        
def parse_tabular_blast(tabular_blast_file):

    #value indexes
    with open(tabular_blast_file, 'r') as blast_file:
        for line in blast_file:
            lst=line.split()
            qseqid=lst[0]
            sseqid=lst[1]
            pident=lst[2]
            length=lst[3]
            mismatch=lst[4]
            gapopen=lst[5]
            qstart=lst[6]
            qend=lst[7]
            sstart=lst[8]
            send=lst[9]
            evalue=lst[10]
            bitscore=lst[11]
            yield Tabular_Blast(qseqid,sseqid,pident,length,mismatch,gapopen,
                                qstart,qend,sstart,send,evalue,bitscore)
def parse_tabular_blast_query_subject(tabular_blast_file):
    """Generator that yields a tuple of the query gi and the subject gi"""
    with open(tabular_blast_file, 'r') as blast_file:
        for line in blast_file:
            lst=line.split()
            query=lst[0]
            query_gi=query.split("|")[1]
            subject=lst[1]
            subject_gi=subject.split("|")[1]
            yield (query_gi, subject_gi)

def averagable_values_dic(blast_result):
    """Input: 1 tabular blast results.
        Output: A dictionary where the hits are indexed by a
                frozen set of the sequence ids and the values are a dictionary
                of the averagable values"""
    blast_result_dic={}
    for record in parse_tabular_blast(blast_result):
        dic_key=frozenset([record.qseqid,record.sseqid]) #won't eleminate self hits
        dic_value={"pident":record.pident,
                   "length":record.length,
                   "mismatch":record.mismatch,
                   "gapopen":record.gapopen,
                   "evalue":record.evalue,
                   "bitscore":record.bitscore}
        blast_result_dic[dic_key]=dic_value
    return blast_result_dic

def averagable_values_shelve(blast_result,out_file):
    db=shelve.open(out_file)
    for record in parse_tabular_blast(blast_result):
        if set([record.qseqid,record.sseqid])==1:
            continue
        db_key=str(record.qseqid)+" "+str(record.sseqid)
        db_value={"pident":record.pident,
                   "length":record.length,
                   "mismatch":record.mismatch,
                   "gapopen":record.gapopen,
                   "evalue":record.evalue,
                   "bitscore":record.bitscore}
        db[db_key]=db_value
    db.close()

def avg_reciporical_hits(blast_result1,blast_result2,output_shelve):
    """Input: 2 tabular blast results.
        Output: A shelve where the reciporical hits are indexed by a
                frozen set of the sequence ids and the values are a dictionary
                of the averaged values"""
    blast_result1_dic=averagable_values_dic(blast_result1)
    blast_result2_dic=averagable_values_dic(blast_result2)
    db=shelve.open(output_shelve)
    for key in blast_result1_dic:
        if key in blast_result2_dic:
            dic={}
            for value_key in blast_result1_dic[key]:
                dic[value_key]=float(blast_result1_dic[key][value_key]+blast_result2_dic[key][value_key])/2
            db[str(key)]=dic #Must do str(key) because frozenset cannot be a key in a shelve.
        else:
            continue
    db.close()

            
def create_tabular_blast_shelve(tabular_blast_file, shelve_file):
    db=shelve.open(shelve_file)
    for record in parse_tabular_blast(tabular_blast_file):
        key=record.get_query_gi()+" "+record.get_subject_gi()
        db[key]=record
    db.close()

def create_query_subject_pickle_set(tabular_blast_file,pickle_file):
    """Input: a blast results in tabular format and a file handle
            to write the output to.
       Output: A pickled set of strings in "query subject" form"""		
    query_subject_sets=set()
    for record in parse_tabular_blast_query_subject(tabular_blast_file):
        query_gi=record[0]
        subject_gi=record[1]
        set_string=query_gi+" "+subject_gi
        query_subject_sets.add(set_string)
    set_file=open(pickle_file, 'wb') #write as binary
    pickle.dump(query_subject_sets, set_file)
    set_file.close()

def write_to_file(list_of_objects, output_file):
    """Input: A list of Tabular_Blast objects
       Output: A file in Tabular blast format"""
    with open(output_file, "w") as o:
        for record in list_of_objects:
            o.write("%s %s %.2f %i %i %i %i %i %i %i %.2f %.2f\n"%record.tabular_tuple())

def filter_by_gi_set(filter_set,blast_file):
    """Returns 2 list of tabular blast objects
        the first it the sequence id frozen set is in the set
        and the second if it is not in the set"""
    in_set=[]
    not_in_set=[]
    for record in parse_tabular_blast(blast_file):
        set_check=frozenset([record.get_query_gi(),record.get_subject_gi()])
        if set_check in filter_set:
            in_set.append(record)
        else:
            not_in_set.append(record)
    return (in_set,not_in_set)
    

if __name__=="__main__":
    blast_result=Tabular_Blast("Q1", "S1", "65", "100", "3", "1", "25", "100",
                               "30", "100", "1e-15", "500")
    blast_result2=Tabular_Blast("gi|89024442|gb|DV663271.1|DV663271","gi|118131130|ref|NM_001009935.2|",97.92, 673,13,1,
                                 1, 673,22,693,0.0,1186)
    blast_result_file="try_est_results.txt"
    
    print blast_result
    print blast_result2
    print blast_result2.get_query_gi()
    print blast_result2.get_subject_gi()
    print len(blast_result2.tabular_tuple())
    print "\n"
    average_results=Blast_Averages(blast_result,blast_result2)
    print average_results.avg_pident
    print average_results.avg_length
    print average_results.avg_mismatch
    print average_results.avg_gapopen
    print average_results.avg_evalue
    print average_results.avg_bitscore
    #write_to_file([blast_result2, blast_result2],'/home/justin/test2/test.out')

    #averagable_values_shelve('/home/justin/Homology_test_set/test_query_output.txt',"averagable_values.db")

    #create_tabular_blast_shelve(blast_result_file,"try_est_results.db")
    #create_query_subject_pickle_set(blast_result_file, "try_est_results.pickle")
    
